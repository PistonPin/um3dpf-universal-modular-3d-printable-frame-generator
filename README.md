# UM3DPF - universal modular 3d-printable frame generator 

## What

This generator can be used to create 3D-printable frame structures for all kinds of things, like electronics boxes (perhaps even a frame for a small 3D printer), Mario cubes, etc. :) (You can also create other rectangular cuboids, not just cubes.)

It has a hole pattern all around it in 20mm increments, where you can (create and) attach all kinds of things. One reason to choose a distance of 20mm was that quite a few components that are meant for 20xx (2020, 2040, etc.) aluminum extrusion profiles could either be directly attached or modified to fit with little effort.

![A technical drawing showing all critical dimensions.](./img/6x6x6_demo_frame.png "A technical drawing showing all critical dimensions.")

## A word of warning

**Consider this experimental!** I've used it to generate a small test frame, and everything works as intended, but it would be a good idea to a) visually check the generated model for sanity and b) print a test piece (ideally a corner, cut in your preferred slicer).

## How

Download and open the file `UM3DPF - universal modular 3d-printable frame.scad` in [OpenSCAD](https://openscad.org).

Un-hide the customizer.

![Un-hide the customizer.](./img/unhide_customizer.png "Screenshot of the menu, where you can un-hide the customizer.")

Change the customizer settings to your liking.

![Change the customizer settings to your liking.](./img/customizer_settings.png "Screenshot of the customizer with all available parameters.")

Choose if you want a "tank frame" or not (default = regular frame). The regular frame panels are only joined in the corners. The "tank frame" lets you add additional assembly holes. Here, three per side.

![Choose if you want a tank "frame" or not.](./img/1_tank_frame_3_assembly_holes.png "Screenshot showing an even sturdier frame panel.")

But be careful if you generate a "tank frame" with additional assembly holes! They could collide with the attachment holes!

![But be careful if you generate a "tank frame"!](./img/2_assembly_attachment_hole_collision.png "Screenshot of a collision of an assembly and and attachment hole.")

I'm sure there are things that could be solved better or in a more elegant way. Never the less, I hope this comes in handy for someone. :)