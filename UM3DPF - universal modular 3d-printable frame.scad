/* ########################

Version 0.1

This framework uses tmu[s], which stands for twenty millimeter unit[s] (not very creative, I know :)).

1tmu equals 20mm - This makes it easy to later create attachments for these frames, since they only need to use a hole pattern of 20mm increments. (It also should make adapting components for 20xx aluminium extrusion profiles fairly straight forward.)

######################## */

// -------------------- Variables

// Frame WIDTH in tmu (1tmu = 20mm)
tmuWidth = 4;

// Frame HEIGHT in tmu (1tmu = 20mm)
tmuHeight = 4;

// Tank frame? If this is set to "yes", it will generate an even sturdier frame with the option to add more holes to join the frame panels together.
tankFrame = "no"; // [no, yes]

// Number of assembly holes (= holes required to assemble the frame) along each side. Change these to suit your needs: Increase for a sturdier frame (might be necessary for larger frames), decrease for less M3 hardware. This doesn't change the 20mm bolt pattern, so check for collision with the attachment holes (= holes on the outside of the frame, where you attach things to).

// Tank frame only: Number of assembly holes WIDTH - IMPORTANT! Check for collision with the attachment holes!
assemblyHolesWidth = 3;

// Tank frame only: Number of assembly holes HEIGHT - IMPORTANT! Check for collision with the attachment holes!
assemblyHolesHeight = 3;

// M3 bolt hole diameter
M3boltHoleDiameter = 3.4;

// M3 hex nut width - IMPORTANT! Before printing any frame panels, make sure that the diameter of the head of the bolts you want to use is equal to or smaller than the width of the hex nuts!
M3hexNutWidth = 5.5;

// -------------------- DON'T CHANGE THE FOLLOWING CODE! (Unless you want to tinker … :))

module __Customizer_Limit__ () {}  // Hide the following variables from the customizer

frameWidth = tmuWidth * 20;
frameHeight = tmuHeight * 20;
frameThickness = 10;

module basicFrameShape() {
    hull() {
        translate([0, 0, 1.5 / 2 + 0.5]) {
            cube([frameWidth - 2 * 2, frameHeight - 2 * 2 - 2 * 2, 1.5], center = true);
            cube([frameWidth - 2 * 2 - 2 * 2, frameHeight - 2 * 2, 1.5], center = true);

            translate([0, 0, -0.5]) {
                cube([frameWidth - 2 * 2 - 1, frameHeight - 2 * 2 - 2 * 2 - 1, 1.5], center = true);
                cube([frameWidth - 2 * 2 - 2 * 2 - 1, frameHeight - 2 * 2 - 1, 1.5], center = true);
            }
        }

        translate([0, 0, frameThickness / 2]) {
            cube([frameWidth - 2 * 2 - 2 * 8, frameHeight - 2 * 2 - 2 * 8, frameThickness], center = true);
        }
    }
}

module frameWindow() {
    union() {
        translate([0, 0, 5]) {
            cube([frameWidth - 2 * 20, frameHeight - 2 * 20, 12], center = true);
        }

        hull() {
            cube([frameWidth - 2 * 20, frameHeight - 2 * 20, 1], center = true);

            translate([0, 0, -0.5]) {
                cube([frameWidth - 2 * 20 + 1, frameHeight - 2 * 20 + 1, 1], center = true);
            }
        }
    }
}

module assemblyHardwareCutout(rotation=0) {
    union() {
        cylinder(d = M3boltHoleDiameter / cos(180 / 100), h = 25, $fn = 100);
        rotate([0, 0, rotation]) translate([0, 0, -20]) cylinder(d = M3hexNutWidth / cos(180 / 6), h = 20, $fn = 6);
    }
}

module assemblyHardwareCutoutsWidth() {
    if (tankFrame == "no") {
        translate([((frameWidth - 2 * 20) / 2 ), frameHeight / 2 - 20 / 2 + 1, 10 / 2]) rotate([-45, 0, 0]) assemblyHardwareCutout();
        mirror([1, 0, 0]) translate([((frameWidth - 2 * 20) / 2 ), frameHeight / 2 - 20 / 2 + 1, 10 / 2]) rotate([-45, 0, 0]) assemblyHardwareCutout();
    } else {
        translate([-(((frameWidth - 2 * 20) / (assemblyHolesWidth - 1) ) + ((frameWidth - 2 * 20) / 2 )), 0, 0]) {
            for (a =[1:assemblyHolesWidth]) {
                translate([((frameWidth - 2 * 20) / ( assemblyHolesWidth - 1) ) * a, frameHeight / 2 - 20 / 2 + 1, 10 / 2]) rotate([-45, 0, 0]) assemblyHardwareCutout();
            }
        }
    }
}

module assemblyHardwareCutoutsHeight() {
    if (tankFrame == "no") {
        translate([frameWidth / 2 - 20 / 2 + 1, ((frameHeight - 2 * 20) / 2 ), 10 / 2]) rotate([0, 45, 0]) assemblyHardwareCutout(rotation=90);
        mirror([0, 1, 0]) translate([frameWidth / 2 - 20 / 2 + 1, ((frameHeight - 2 * 20) / 2 ), 10 / 2]) rotate([0, 45, 0]) assemblyHardwareCutout(rotation=90);
    } else {
        translate([0, -(((frameHeight - 2 * 20) / (assemblyHolesHeight - 1) ) + ((frameHeight - 2 * 20) / 2 )), 0]) {
            for (a =[1:assemblyHolesHeight]){
                translate([frameWidth / 2 - 20 / 2 + 1, ((frameHeight - 2 * 20) / ( assemblyHolesHeight - 1) ) * a, 10 / 2]) rotate([0, 45, 0]) assemblyHardwareCutout(rotation=90);
            }
        }
    }
}

module attachmentHardwareCutout(rotation=0) {
    translate([0, 0, 3]) {
        union() {
            rotate([0, 0, rotation]) cylinder(d = M3hexNutWidth / cos(180 / 6), h = 20, $fn = 6);
            translate([0, 0, -4]) cylinder(d = M3boltHoleDiameter / cos(180 / 100), h = 20, $fn = 100);
            translate([0, 0, -4.5]) cylinder(d1 = M3boltHoleDiameter/cos(180 / 100) + 4, d2 = M3boltHoleDiameter / cos(180 / 100), h = 2, $fn = 100);
        }
    }
}

module attachmentHardwareCornerCutouts() {
    translate([frameWidth / 2 - 10, frameHeight / 2 -10, 0]) attachmentHardwareCutout(rotation = -45);
    mirror([1, 0, 0]) translate([frameWidth / 2 - 10, frameHeight / 2 -10, 0]) attachmentHardwareCutout(rotation = -45);
}

module attachmentHardwareCutoutsWidth() {
    translate([-(frameWidth / 2) + 10, 0, 0]) {
        for (a =[1:tmuWidth - 2]){
            translate([a * 20, frameHeight / 2 - 10, 0]) attachmentHardwareCutout(rotation = 60);
        }
    }
}

module attachmentHardwareCutoutsHeight() {
    translate([0, -(frameHeight / 2) + 10, 0]) {
        for (a =[1:tmuHeight - 2]){
            translate([frameWidth / 2 - 10, a * 20, 0]) attachmentHardwareCutout(rotation = 90);
        }
    }
}

module framePanel() {
    difference() {
        basicFrameShape();
        frameWindow();
        
        // Assembly hardware
        assemblyHardwareCutoutsWidth();
        mirror([0, 1, 0]) assemblyHardwareCutoutsWidth();

        assemblyHardwareCutoutsHeight();
        mirror([1, 0, 0]) assemblyHardwareCutoutsHeight();

        // Attachment hardware
        attachmentHardwareCornerCutouts();
        mirror([0, 1, 0]) attachmentHardwareCornerCutouts();
        
        attachmentHardwareCutoutsWidth();
        mirror([0, 1, 0]) attachmentHardwareCutoutsWidth();
        
        attachmentHardwareCutoutsHeight();
        mirror([1, 0, 0]) attachmentHardwareCutoutsHeight();
        
        // Corner cutouts
        translate([frameWidth / 2 - 10, frameHeight / 2 - 10 - (M3hexNutWidth * 1.25 / sin(90)) * sin(45), 6]) rotate([0, 0, 45]) cube([M3hexNutWidth * 1.25 * 2, M3hexNutWidth * 1.25, 10]);
        mirror([1, 0, 0]) translate([frameWidth / 2 - 10, frameHeight / 2 - 10 - (M3hexNutWidth * 1.25 / sin(90)) * sin(45), 6]) rotate([0, 0, 45]) cube([M3hexNutWidth * 1.25 * 2, M3hexNutWidth * 1.25, 10]);
        mirror([0, 1, 0]) {
            translate([frameWidth / 2 - 10, frameHeight / 2 - 10 - (M3hexNutWidth * 1.25 / sin(90)) * sin(45), 6]) rotate([0, 0, 45]) cube([M3hexNutWidth * 1.25 * 2, M3hexNutWidth * 1.25, 10]);
            mirror([1, 0, 0]) translate([frameWidth / 2 - 10, frameHeight / 2 - 10 - (M3hexNutWidth * 1.25 / sin(90)) * sin(45), 6]) rotate([0, 0, 45]) cube([M3hexNutWidth * 1.25 * 2, M3hexNutWidth * 1.25, 10]);
        }
        
        if (tankFrame == "no") {
            // Cutouts to lighten the frame and reduce print time
            translate([0, 0, 10]) cube([frameWidth - (2 * 20 + M3hexNutWidth * 1.5), frameHeight, 9], center = true);
            translate([0, 0, 10]) cube([frameWidth, frameHeight - (2 * 20 + M3hexNutWidth * 1.5), 9], center = true);
            translate([0, 0, 10]) cube([frameWidth - 2 * 20 + 10, frameHeight - 2 * 20 + 10, 9], center = true);
        }
    }
}

framePanel();